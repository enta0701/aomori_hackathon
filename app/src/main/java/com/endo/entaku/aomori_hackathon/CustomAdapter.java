package com.endo.entaku.aomori_hackathon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<CustomData> {
    private LayoutInflater layoutInflater_;

    public CustomAdapter(Context context, int textViewResourceId, List<CustomData> objects) {
        super(context, textViewResourceId, objects);
        layoutInflater_ = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 特定の行(position)のデータを得る
        CustomData item = (CustomData)getItem(position);

        // convertViewは使い回しされている可能性があるのでnullの時だけ新しく作る
        if (null == convertView) {
            convertView = layoutInflater_.inflate(R.layout.list, null);
        }


        TextView yName;
        yName = (TextView)convertView.findViewById(R.id.yourName);
        yName.setText(item.getYourName());

        TextView cText;
        cText = (TextView)convertView.findViewById(R.id.commentText);
        cText.setText(item.getCommentText());

        TextView dTime;
        dTime = (TextView)convertView.findViewById(R.id.dateTime);
        dTime.setText(item.getDateTime());

        TextView mName;
        mName = (TextView)convertView.findViewById(R.id.myName);
        mName.setText(item.getMyName());

        ImageView fName;
        fName = (ImageView)convertView.findViewById(R.id.filename);
        fName.setImageBitmap(item.getFile());

        return convertView;
    }
}