package com.endo.entaku.aomori_hackathon;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.ImageView;

import com.nifty.cloud.mb.FindCallback;
import com.nifty.cloud.mb.GetDataCallback;
import com.nifty.cloud.mb.NCMB;
import com.nifty.cloud.mb.NCMBException;
import com.nifty.cloud.mb.NCMBFile;
import com.nifty.cloud.mb.NCMBObject;
import com.nifty.cloud.mb.NCMBQuery;

import java.util.ArrayList;
import java.util.List;


public class Rank extends ActionBarActivity {

    private ImageView rankNo1;
    String TAG = "ログ";
    Bitmap reimg, bmp,bmp2, resultBit;
    Matrix matrix;
    ArrayList<Bitmap> list;
    GridView gridView;
    BitmapAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank);
        NCMB.initialize(this, "9dcf8a2c1df88587ba60521e392ba470777109b151324cc1542a2246edffc850",
                "dbca9b220d430bdb2afe96ebb499a9b588ab6a7e35b0ef8d777f69620b6f8a12");


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.icon);

        rankNo1 = (ImageView) findViewById(R.id.rank_no1);
        NCMBQuery query = NCMBQuery.getQuery("Aomori_hackathon");

        list = new ArrayList<>();
        query.findInBackground(new FindCallback<NCMBObject>() {
            @Override
            public void done(List<NCMBObject> result, NCMBException e) {
                DisImage(result.get(0));

                for (int i = 11; i < 14; i++) {
                    DisImage2(result.get(i));
                }
            }
        });


        adapter = new BitmapAdapter(getApplicationContext(), R.layout.list_img, list);
        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(adapter);

    }

    private void DisImage(NCMBObject image) {
        matrix = new Matrix();

        // 拡大比率
        float rsz_ratio_w = 4.0f;
        float rsz_ratio_h = 4.0f;

        matrix.postScale(rsz_ratio_w, rsz_ratio_h);

        String imgStr = image.getString("commentimg");
        NCMBFile fileData = new NCMBFile(imgStr);
        fileData.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] obj, NCMBException e) {
                // TODO Auto-generated method stub
                if (e != null) {
                    Log.d(TAG, "error in getdata");
                } else {
                    // TODO Auto-generated method stub
                    bmp = BitmapFactory.decodeByteArray(obj, 0, obj.length);
                    reimg = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
                    rankNo1.setImageBitmap(reimg);
                }
            }

        });
    }

    private void DisImage2(NCMBObject image) {

        String imgStr = image.getString("commentimg");
        NCMBFile fileData = new NCMBFile(imgStr);
//        fileData.getDataInBackground(new GetDataCallback() {
//            @Override
//            public void done(byte[] obj, NCMBException e) {
//                // TODO Auto-generated method stub
//                if (e != null) {
//                    Log.d(TAG, "error in getdata");
//                } else {
//                    // TODO Auto-generated method stub
////                    bmp2 = BitmapFactory.decodeByteArray(obj, 0, obj.length);
//                }
//            }
//        });
//        bmp2 = null;
//        try {
//            bmp2 = BitmapFactory.decodeByteArray(fileData.getData(), 0, fileData.getData().length);
//        }catch(NCMBException e){
//            e.printStackTrace();
//        }
        String url = fileData.getURL();
        Log.d(TAG,url);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_post) {
            Intent intent = new Intent(Rank.this, MainActivity.class);
            // 次画面のアクティビティ起動
            startActivity(intent);
            return true;
        }

        if (id == R.id.action_favorite) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
