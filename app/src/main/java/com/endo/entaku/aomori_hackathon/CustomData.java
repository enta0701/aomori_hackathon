package com.endo.entaku.aomori_hackathon;

import android.graphics.Bitmap;

/**
 * Created by entaku on 15/01/17.
 */
public class CustomData {

    private String yourName_;
    private String commentText_;
    private String dateTime_;
    private String myName_;
    private Bitmap mFile;


    public void setYourName(String text) {
        yourName_ = text;
    }

    public String getYourName() {
        return yourName_;
    }

    public void setCommentText(String text) {
        commentText_ = text;
    }

    public String getCommentText() {
        return commentText_;
    }

    public void setDateTime(String text) {
        dateTime_ = text;
    }

    public String getDateTime() {
        return dateTime_;
    }

    public void setMyName(String text) {
        myName_ = text;
    }

    public String getMyName() {return myName_;}

    public void setFile(Bitmap image) {mFile = image;}

    public Bitmap getFile() {return mFile;}
}
