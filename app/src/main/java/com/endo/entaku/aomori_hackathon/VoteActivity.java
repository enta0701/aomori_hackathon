package com.endo.entaku.aomori_hackathon;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.nifty.cloud.mb.FindCallback;
import com.nifty.cloud.mb.NCMB;
import com.nifty.cloud.mb.NCMBException;
import com.nifty.cloud.mb.NCMBFile;
import com.nifty.cloud.mb.NCMBObject;
import com.nifty.cloud.mb.NCMBQuery;

import java.util.List;


public class VoteActivity extends ActionBarActivity {

    String filename;
    NCMBQuery<NCMBObject> query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.icon);

        NCMB.initialize(this, "9dcf8a2c1df88587ba60521e392ba470777109b151324cc1542a2246edffc850", "dbca9b220d430bdb2afe96ebb499a9b588ab6a7e35b0ef8d777f69620b6f8a12");
        query = NCMBQuery.getQuery("Vote");
        query.findInBackground(new FindCallback<NCMBObject>() {
            @Override
            public void done(List<NCMBObject> scoreList, NCMBException e) {
                if (e == null) {
                    // 成功
                } else {
                    // 失敗
                }
            }
        });

        Intent intent = getIntent();
        filename = intent.getStringExtra("filename");

        ImageView imageView = (ImageView)findViewById(R.id.imageView2);
        NCMBFile fileData = new NCMBFile(filename);
        Bitmap img = null;
        try {
            img = BitmapFactory.decodeByteArray(fileData.getData(), 0, fileData.getData().length);
        }catch(NCMBException e){
            e.printStackTrace();
        }
        imageView.setImageBitmap(img);
        imageView.setVisibility(View.VISIBLE);

        Button button = (Button)findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Vote(new NCMBObject("Vote"),filename);
            }
        });
    }

    public void Vote(NCMBObject object,String filename){
//        object.put("commentimg",filename);
//        object.put("vote",0);
//        try {
//            object.save();
//        }catch(NCMBException e){
//            e.printStackTrace();
//        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vote, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
