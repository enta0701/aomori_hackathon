package com.endo.entaku.aomori_hackathon;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.nifty.cloud.mb.FindCallback;
import com.nifty.cloud.mb.GetDataCallback;
import com.nifty.cloud.mb.NCMB;
import com.nifty.cloud.mb.NCMBException;
import com.nifty.cloud.mb.NCMBFile;
import com.nifty.cloud.mb.NCMBObject;
import com.nifty.cloud.mb.NCMBQuery;
import com.nifty.cloud.mb.ProgressCallback;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ActionBarActivity {

    private CustomAdapter adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    List<CustomData> objects; //表示リスト

    private NCMBQuery<NCMBObject> query;
    int id_count = 0;

    String voteName[] = new String[50];
    int index = 0;

    CustomData item;

    ProgressDialog dialog;

    int loadImgTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NCMB.initialize(this, "9dcf8a2c1df88587ba60521e392ba470777109b151324cc1542a2246edffc850",
                "dbca9b220d430bdb2afe96ebb499a9b588ab6a7e35b0ef8d777f69620b6f8a12");

        dialog = new ProgressDialog(MainActivity.this);
        dialog.setTitle("dialog test");
        dialog.setMessage("処理中");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMax(loadImgTime);
        dialog.setCancelable(false);
        dialog.show();

        // ActionBarの設定
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.icon);

        // 表示リスト
        objects = new ArrayList<>();

        // SwipeRefreshLayoutの設定
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);

        //データ取得
        query = NCMBQuery.getQuery("Aomori_hackathon");
        (new Thread(new Runnable() {
            @Override
            public void run() {
                query.findInBackground(new FindCallback<NCMBObject>() {
                    @Override
                    public void done(List<NCMBObject> result, NCMBException e) {
                        if (e == null) {
                            for (int i = result.size(); i > 0; i--) {
                                if (i != 1) {
                                    ListDisplay(result.get(i - 1), result.get(i - 2));
                                } else {
                                    ListDisplay(result.get(i - 1));
                                }
                            }
                            id_count = result.size();
                            Log.d("ログ", id_count + "");
                        } else {
                            Log.e("エラー", "データ未取得");
                        }
                    }
                });


                dialog.dismiss();
            }
        })).start();


    }

    public void ListDisplay(NCMBObject message) {
        // データの作成

        item = new CustomData();

        item.setYourName("あなたが1番最初のコメントです");
        item.setCommentText(message.getString("comment"));
        item.setDateTime(message.getString("date"));
        item.setMyName(message.getString("user"));

        String filename = message.getString("commentimg");
        voteName[index] = filename;
        index++;

        NCMBFile fileData = new NCMBFile(filename);
        Log.d("", fileData.getURL());
        fileData.getDataInBackground(new GetDataCallback() {

            @Override
            public void done(byte[] obj, NCMBException e) {
                if (e != null) {
                    Log.d("error", "error in getdata");
                } else {
                    Bitmap bmp = BitmapFactory.decodeByteArray(obj, 0, obj.length);
                    item.setFile(bmp);
                }
            }

        }, new ProgressCallback() {
            public void done(Integer percentDone) {
                // 進捗状況。1 - 100 のpercentDoneを返す
//                loadImgTime = percentDone;

            }
        });

//        Bitmap img = null;
//        try {
//            img = BitmapFactory.decodeByteArray(fileData.getData(), 0, fileData.getData().length);
//        } catch(NCMBException e){
//            e.printStackTrace();
//        }
//
//        if(img != null) {
//            item.setFile(img);
//        }

        objects.add(item);

        CustomAdapter customAdapter = new CustomAdapter(this, 0, objects);

        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(customAdapter);

        // リストビューのアイテムがクリックされた時に呼び出されるコールバックリスナーを登録します
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ListView listView = (ListView) parent;
                // クリックされたアイテムを取得します
                String item = listView.getItemAtPosition(position).toString();
                Toast.makeText(MainActivity.this, item, Toast.LENGTH_SHORT).show();
                Vote(voteName[position]);
            }
        });
    }


    public void ListDisplay(NCMBObject message, NCMBObject after_name) {
        // データの作成

        item = new CustomData();

        item.setYourName(after_name.getString("user") + "さんへの返信");
        item.setCommentText(message.getString("comment"));
        item.setDateTime(message.getString("date"));
        item.setMyName(message.getString("user"));

        String filename = message.getString("commentimg");
        voteName[index] = filename;
        index++;


        NCMBFile fileData = new NCMBFile(filename);
        fileData.getDataInBackground(new GetDataCallback (){

            @Override
            public void done(byte[] obj, NCMBException e) {
                if (e != null){
                    Log.d("エラー", "error in getdata");
                } else {
                    Bitmap bmp = BitmapFactory.decodeByteArray(obj, 0, obj.length);
                    item.setFile(bmp);
                }
            }

        }, new ProgressCallback() {
            public void done(Integer percentDone) {
                // 進捗状況。1 - 100 のpercentDoneを返す
                loadImgTime = percentDone;
            }
        });

//        Bitmap img = null;
//        try {
//            img = BitmapFactory.decodeByteArray(fileData.getData(), 0, fileData.getData().length);
//        } catch (NCMBException e) {
//            e.printStackTrace();
//        }
//
//        if (img != null) {
//            item.setFile(img);
//        }


        objects.add(item);

        CustomAdapter customAdapter = new CustomAdapter(this, 0, objects);

        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(customAdapter);

        // リストビューのアイテムがクリックされた時に呼び出されるコールバックリスナーを登録します
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ListView listView = (ListView) parent;
                // クリックされたアイテムを取得します
                String item = listView.getItemAtPosition(position).toString();
                Toast.makeText(MainActivity.this, item, Toast.LENGTH_SHORT).show();
                Vote(voteName[position]);
            }
        });
    }

    public void Vote(String voteName) {
        Intent intent = new Intent(this, VoteActivity.class);
        intent.putExtra("filename", voteName);
        if (!"".equals(voteName)) {
            startActivity(intent);
        }
    }

    public void ListUpdate(NCMBObject message) {
        // データの作成

        CustomData item = new CustomData();

        item.setYourName("あなたが1番最初のコメントです");
        item.setCommentText(message.getString("comment"));
        item.setDateTime(message.getString("date"));
        item.setMyName(message.getString("user"));

        String filename = message.getString("commentimg");
        NCMBFile fileData = new NCMBFile(filename);
        voteName[index] = filename;
        index++;

        Bitmap img = null;
        try {
            img = BitmapFactory.decodeByteArray(fileData.getData(), 0, fileData.getData().length);
        } catch (NCMBException e) {
            e.printStackTrace();
        }

        if (img != null) {
            item.setFile(img);
        }

        objects.add(0, item);

        CustomAdapter customAdapater = new CustomAdapter(this, 0, objects);

        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(customAdapater);
    }

    public void ListUpdate(NCMBObject message, NCMBObject after_name) {
        // データの作成

        CustomData item = new CustomData();

        item.setYourName(after_name.getString("user") + "さんへの返信");
        item.setCommentText(message.getString("comment"));
        item.setDateTime(message.getString("date"));
        item.setMyName(message.getString("user"));

        String filename = message.getString("commentimg");
        NCMBFile fileData = new NCMBFile(filename);
        voteName[index] = filename;
        index++;


        Bitmap img = null;
        try {
            img = BitmapFactory.decodeByteArray(fileData.getData(), 0, fileData.getData().length);
        } catch (NCMBException e) {
            e.printStackTrace();
        }

        if (img != null) {
            item.setFile(img);
        }

        objects.add(0, item);

        CustomAdapter customAdapater = new CustomAdapter(this, 0, objects);

        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(customAdapater);
    }


    //更新処理
    private SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            query.findInBackground(new FindCallback<NCMBObject>() {
                @Override
                public void done(List<NCMBObject> result, NCMBException e) {
                    for (int i = id_count, j = result.size(); i < j; i++) {
                        if (i != 0) {
                            ListUpdate(result.get(i), result.get(i - 1));
                        } else {
                            ListUpdate(result.get(i));
                        }
                    }
                    id_count = result.size();
                }
            });
            // 3秒待機
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }, 3000);
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_post) {
            Intent intent = new Intent(MainActivity.this, PostPage.class);
            startActivity(intent);
            return true;
        }

        if (id == R.id.action_favorite) {
            Intent intent = new Intent(MainActivity.this, Rank.class);
            // 次画面のアクティビティ起動
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
