package com.endo.entaku.aomori_hackathon;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.nifty.cloud.mb.FindCallback;
import com.nifty.cloud.mb.NCMB;
import com.nifty.cloud.mb.NCMBException;
import com.nifty.cloud.mb.NCMBFile;
import com.nifty.cloud.mb.NCMBObject;
import com.nifty.cloud.mb.NCMBQuery;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;


public class PostPage extends ActionBarActivity {

    private static final int RESULT_PICK_IMAGE_FILE = 1001;

    private EditText user_name;
    private EditText comment;
    private ImageButton send;
    private String uName;
    private String cText;
    private NCMBObject ah, bh;
    private NCMBQuery<NCMBObject> query;
    private ImageButton button;
    private Bitmap bmp = null;
    private ImageView selectImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_page);

        NCMB.initialize(this, "9dcf8a2c1df88587ba60521e392ba470777109b151324cc1542a2246edffc850",
                "dbca9b220d430bdb2afe96ebb499a9b588ab6a7e35b0ef8d777f69620b6f8a12");

        // 投稿ボックス
        user_name = (EditText) findViewById(R.id.user_name);
        comment = (EditText) findViewById(R.id.comment);
        send = (ImageButton) findViewById(R.id.post);

        // ギャラリー
        button = (ImageButton) findViewById(R.id.select);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                imageIntent();
            }
        });

        // 選択した画像表示
        selectImg = (ImageView) findViewById(R.id.postImage);


        query = NCMBQuery.getQuery("Aomori_hackathon");

        send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                query.findInBackground(new FindCallback<NCMBObject>() {
                    @Override
                    public void done(List<NCMBObject> result, NCMBException e) {
                        uName = user_name.getText().toString();
                        cText = comment.getText().toString();
                        PostData(uName, cText, DateTime(), result);
                    }
                });
            }
        });
    }

    public String DateTime() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        String date = year + "/" + (month + 1) + "/" + day + " "
                + hour + ":" + minute;

        return date;
    }


    private void PostData(String user_name, String comment, String date, List<NCMBObject> result) {

        ah = new NCMBObject("Aomori_hackathon");

        String file = imageUpload();
        ah.put("id", result.size() + 1);
        ah.put("comment", comment);
        ah.put("date", date);
        ah.put("weather", "晴れ");
        ah.put("user", user_name);
        ah.put("commentimg", file);
        ah.put("vote", 0);

        bh = new NCMBObject("Vote");
        bh.put("commentimg", file);
        bh.put("vote", 0);


        try {
            ah.save();
            bh.save();
        } catch (NCMBException e) {
            e.printStackTrace();
        }
        imageUpload();
        Toast.makeText(this, "投稿が完了しました", Toast.LENGTH_LONG).show();
        finish();
    }

    // ギャラリー呼び出し
    public void imageIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, RESULT_PICK_IMAGE_FILE);
    }


    //ギャラリーの選択処理
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == RESULT_PICK_IMAGE_FILE && resultCode == RESULT_OK && null != intent) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            bmp = null;
            Uri selectedImageURI = intent.getData();

            InputStream input;
            try {
                input = getContentResolver().openInputStream(selectedImageURI);
                bmp = BitmapFactory.decodeStream(input, null, options);
                selectImg.setImageBitmap(bmp);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    //日時取得
    public String FileName() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);    // 0 - 11
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);
        String date = year + "" + (month + 1) + "" + day + "" + hour + "" + minute + "" + second + "";
        return date;
    }


    public String imageUpload() {
        String filename = "";
        if (bmp != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] bytes = baos.toByteArray();
            filename = FileName() + ".png";
            NCMBFile file = new NCMBFile(filename, bytes);

            try {
                file.save();
            } catch (NCMBException e) {
                e.printStackTrace();
            }
            baos.reset();
            bmp = null;
        }
        return filename;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_post_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
